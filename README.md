#Sujet 4 : tableaux

### Opérations correctes sur les tableaux ?

Donner une trace de cet algorithme, tout en déterminant les lignes
incorrectes.

    Algo exoTableaux
    Variables 
        tab1, tab2 : tableau de 3 entier
        tab3       : tableau de 3 booleen
    Debut
        tab1[0] <-  3 ; tab1[1] <-  2 ; tab1[2] <-  1

        tab2 <- tab1

        si  tab1 < tab2 alors afficher ("tableau 1 petit") finSi

        tab3[1] <- faux 
        tab3[tab1[1]] <- vrai
        tab3[1] <- tab1[1] == tab2[2]

        pour i dans 0..2 faire 
           tab3[i] <- tab1[i] == tab2[i] 
        finPour
    Fin exoTableaux

### Traitements sur un tableau

1.  Ecrire une procédure
    `afficherTabEntiers (t: tableau d’entiers) void` qui
    affiche les éléments du tableau en paramètre.

2.  Ecrire une fonction
    `saisirTabEntiers (taille : entier) retourne tableau d’entiers` qui
    crée un tableau de `taille` entiers, fait saisir ses éléments par
    l'utilisateur et le retourne.

3.  Ecrire une fonction `moyenne (t: tableau d’entiers) retourne reel`
    qui retourne la moyenne des éléments du tableau en paramètre.

4.  En faisant appel aux fonctions précédentes, écrire une procédure
    `traiterTabEntiers() void `  qui :

    1.  initialise un tableau `tab` de 100 entiers en le faisant remplir
        par l'utilisateur ;

    2.  affiche les valeurs de `tab` ;

    3.  calcule et affiche la moyenne des éléments de `tab` ;

    4.  modifie `tab` en ajoutant 1 à chaque élément pair de `tab` ;

    5.  affiche les éléments du tableau `tab`n` modifié.

### Fréquence des chiffres dans un nombre

1.  Ecrire une fonction `frequenceChiffres` qui prend un nombre entier
    naturel `n` en paramètre et renvoie un tableau de 10 entiers. Chaque
    case de ce tableau indicé de 0 à 9 contient la fréquence (le nombre
    d'occurrences) du chiffre en indice dans le nombre `n`. Cela permet
    de savoir combien de 0 contient le nombre, combien de 1, etc.

    Par exemple, si `n = 15121`, alors la fonction renvoie le tableau
    ` [0,3,1,0,0,1,0,0,0,0]`.

    Indication : diviser `n` par 10 itérativement.

2.  Ecrire une fonction `aChiffresTousDifferents` qui renvoie `vrai` si
    les chiffres (de la représentation en base 10) d'un entier `n` en
    paramètre sont distincts 2 à 2 (tous différents entre eux), `faux`
    sinon. Cette fonction fera appel à la fonction précédente.

    Par exemple, si $n = 15121$, alors la fonction renvoie `faux` car le
    chiffre 1 apparaît plusieurs fois.

3.  Ecrire une fonction `aChiffresTousDifferentsBis` qui a exactement
    les mêmes spécifications que `aChiffresTousDifferents`, mais qui est
    plus efficace en temps et ne fait pas appel à `frequenceChiffres`.


### Palindrome

Ecrire une fonction booléenne `estPalindrome` qui détermine si les
caractères contenus dans un tableau de caractères constituent un
palindrome.

Exemples : ELLE et KAYAK sont des palindromes. RESTER n'est pas un
palindrome.

On écrira une deuxième fonction `testPalin` qui appelle ` estPalindrome`
pour la tester. En Java, on pourra utiliser ` Ut.saisirCharArray()` qui
demande de saisir une chaîne de caractères sur le terminal et retourne
un tableau de caractères.

