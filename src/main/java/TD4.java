/**
 * affiche les éléments du tableau t
 * @param t
 */
void afficherTabEntiers (int []t){
    throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
}

/**
 *
 * @param taille
 * @return un tableau de taille entiers ( fait saisir ses éléments par l’utilisateur)
 *
 */
int[] saisirTabEntiers (int taille ) {
    throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
}

/**
 *
 * @param t
 * @return  la moyenne des éléments du tableau en paramètre.
 */
 double moyenne (int []t ) {
        throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
    }


/**
 *initialise un tableau `tab` de 100 entiers en le faisant remplir par l'utilisateur ;
 *affiche les valeurs de `tab` ;
 *calcule et affiche la moyenne des éléments de `tab` ;
 *modifie `tab` en ajoutant 1 à chaque élément pair de `tab` ;
 *affiche les éléments du tableau `tab`n` modifié.
 */
   void traiterTabEntiers(){
       throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
   }

/**
 *
 *
 * @param n
 * @return un tableau de 10 entiers.  Chaque case de ce tableau indicé de 0 à 9 contient la fréquence (le nombre d’occurrences)
 * du chiffre en indice dans le nombre n.
 *  *
 */
int [] frequenceChiffres(int n)
     {
        throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");

    }

/**
 *Cette fonction fera appel à la fonction précédente.
 * @param n
 * @return `vrai` si les chiffres (de la représentation en base 10) d'un entier `n` en
 * paramètre sont distincts 2 à 2 (tous différents entre eux), `faux` sinon.
 */
boolean aChiffresTousDifferents(int n){
    throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
}

/**
 * ne fait pas appel à `frequenceChiffres`.
 * @param n
 * @return `vrai` si les chiffres (de la représentation en base 10) d'un entier `n` en
 *  * paramètre sont distincts 2 à 2 (tous différents entre eux), `faux` sinon.
 */
 boolean aChiffresTousDifferentsBisV2(int n){
     throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
 }

/**
 *
 * @param mot
 * @return vrai si le mot mot est un palindrome, faux sinon
 */
boolean estPalindrome(char []mot ) {
       throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
 }

 void main ( ){


    }
